﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Infivex.Domain.Models;
using Infivex.Repository.User;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Infivex.Authentication.Services
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _configuration;

        private int FailedPasswordAttemptMaxCount
        {
            get
            {
                return _configuration.GetValue<int>("FailedPasswordAttemptCount");
            }
        }

        public ResourceOwnerPasswordValidator(IUserRepository userRepository, IConfiguration configuration)
        {
            _userRepository = userRepository;
            _configuration = configuration;
        }

        /// <summary>
        /// Validates the resource owner password credential
        /// </summary>
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                var user = await _userRepository.FindByUsernameAsync(context.UserName);
                if (user != null)
                {
                    if (user.FailedPasswordAttemptCount != 5)
                    {
                        // User is not locked
                        if (_userRepository.VerifyPasswordHash(context.Password, user.PasswordHash, user.PasswordSalt))
                        {
                            // Password is correct
                            context.Result = new GrantValidationResult(
                                subject: user.Id.ToString(),
                                authenticationMethod: "custom",
                                claims: GetUserClaims(user));

                            // Reset failed password attempt count
                            await _userRepository.ResetFailedPasswordAttemptCountAsync(user);

                            return;
                        }

                        // Increase failed login attempt counter
                        await _userRepository.IncreaseFailedPasswordAttemptCountAsync(user, 1);

                        if (user.FailedPasswordAttemptCount == FailedPasswordAttemptMaxCount)
                        {
                            // User is locked with the current login attempt
                            await _userRepository.LockUserAccount(user);
                        }

                        context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
                        return;
                    }
                    else
                    {
                        context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
                        return;
                    }
                }
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
                return;
            }
            catch
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
            }
        }

        /// <summary>
        /// Builds claims array from user data
        /// </summary>
        public static Claim[] GetUserClaims(TUser user)
        {
            return new Claim[]
            {
                new Claim("username", user.UserName ?? ""),
                new Claim(JwtClaimTypes.Name, user.FirstName + user.LastName ?? ""),

                // Roles
                // TODO - hardcoded role
                new Claim(JwtClaimTypes.Role, Roles.Administrator.ToString())
            };
        }

        /// <summary>
        /// Roles for IdentityServer
        /// </summary>
        public enum Roles
        {
            Administrator,
            User
        }
    }
}
