﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Infivex.Repository.User;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infivex.Authentication.Services
{
    public class ProfileService : IProfileService
    {
        private readonly IUserRepository _userRepository;

        public ProfileService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        //Get user profile date in terms of claims when calling /connect/userinfo
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            if (context.Client.AllowedGrantTypes.Contains(GrantType.ClientCredentials) &&
                context.Client.AllowedGrantTypes.Contains(GrantType.ResourceOwnerPassword))
            {
                var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject);

                if (!string.IsNullOrEmpty(userId?.Value))
                {
                    //get user from db (find user by user id)
                    var user = await _userRepository.FindByIdAsync(userId.Value);

                    // issue the claims for the user
                    if (user != null)
                    {
                        var claims = ResourceOwnerPasswordValidator.GetUserClaims(user);

                        context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
                    }
                }
            }
            else
            {
                throw new NotSupportedException("Client has insufficient grant types");
            }
        }

        //check if user account is active.
        public async Task IsActiveAsync(IsActiveContext context)
        {
            var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject);

            if (userId != null && !string.IsNullOrWhiteSpace(userId.Value))
            {
                context.IsActive = await _userRepository.IsUserActiveByIdAsync(userId.Value);
            }
            else
                context.IsActive = false;
        }
    }
}
