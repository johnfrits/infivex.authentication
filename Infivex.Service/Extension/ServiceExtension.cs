﻿using IdentityServer4.Services;
using IdentityServer4.Validation;
using Infivex.Authentication.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infivex.Service.Extension
{
    public static class ServiceExtension
    {
        public static IServiceCollection ServiceDescriptors(this IServiceCollection services, string connectionString)
        {
            //Get assembly name in Auth Infrastructure
            var migrationsAssembly = "Infivex.Auth.Infrastructure";
            services.AddIdentityServer()
                // TODO: needs to be replaced by some persistent key material for production scenarios. See the cryptography docs for more information. Use AddSigningCredential
                .AddDeveloperSigningCredential()

                // this adds the config data from DB (clients e.g. web site, resources e.g. API)
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                })

                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30;
                })
                .AddProfileService<ProfileService>();


            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            services.AddTransient<IProfileService, ProfileService>();

            return services;
        }
    }
}
