﻿using Infivex.Infrastructure.DBContext;
using Infivex.Repository.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infivex.Configuration
{
    public static class DependencyResolver
    {
        public static IServiceCollection ConfigurationServiceDescriptors(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddDbContext<WorkbenchMasterContext>(options => options.UseSqlServer(connectionString));

            return services;
        }
    }
}
