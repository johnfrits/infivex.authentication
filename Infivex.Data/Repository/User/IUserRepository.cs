﻿using Infivex.Domain.Models;
using System.Threading.Tasks;

namespace Infivex.Repository.User
{
    public interface IUserRepository
    {
        bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt);
        Task<TUser> FindByUsernameAsync(string userId);
        Task<TUser> FindByIdAsync(string userId);
        Task<bool> IsUserActiveByIdAsync(string userId);
        Task<TUser> IncreaseFailedPasswordAttemptCountAsync(TUser user, int amount);
        Task<TUser> ResetFailedPasswordAttemptCountAsync(TUser user);
        Task<TUser> LockUserAccount(TUser user);
    }
}
