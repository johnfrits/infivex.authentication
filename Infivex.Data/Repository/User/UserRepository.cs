﻿using Infivex.Domain.Enums;
using Infivex.Domain.Models;
using Infivex.Infrastructure.DBContext;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Infivex.Repository.User
{
    public class UserRepository : IUserRepository
    {
        public readonly WorkbenchMasterContext workbenchMasterContext;

        public UserRepository(WorkbenchMasterContext workbenchMasterContext)
        {
            this.workbenchMasterContext = workbenchMasterContext;
        }

        public bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                        return false;
                }
            }
            return true;
        }

        public async Task<TUser> FindByIdAsync(string userId)
        {
            var user = await workbenchMasterContext.TUser.FirstOrDefaultAsync(x => x.Id.ToString().Equals(userId));

            if (user != null && user.Status != (int)Status.Deleted)
            {
                if (user.Status == (int)Status.Active)
                    return user;
            }

            return null;
        }

        public async Task<TUser> FindByUsernameAsync(string username)
        {
            var user = await workbenchMasterContext.TUser.FirstOrDefaultAsync(x => x.UserName == username);

            if (user != null && user.Status != (int)Status.Deleted)
            {
                if (user.Status == (int)Status.Active)
                    return user;
            }

            return null;
        }

        public async Task<bool> IsUserActiveByIdAsync(string userId)
        {
            var user = await FindByIdAsync(userId);
            if (user != null && user.FailedPasswordAttemptCount != 5)
                return true;
            return false;
        }

        public async Task<TUser> IncreaseFailedPasswordAttemptCountAsync(TUser user, int amount)
        {
            if (amount != 0)
            {
                user.FailedPasswordAttemptCount += amount;
                await workbenchMasterContext.SaveChangesAsync();
            }
            return user;
        }

        public async Task<TUser> ResetFailedPasswordAttemptCountAsync(TUser user)
        {
            user.FailedPasswordAttemptCount = 0;
            await workbenchMasterContext.SaveChangesAsync();
            return user;
        }

        public async Task<TUser> LockUserAccount(TUser user)
        {
            user.FailedPasswordAttemptCount = 5;
            await workbenchMasterContext.SaveChangesAsync();
            return user;
        }
    }
}
