﻿using Microsoft.Extensions.Configuration;

namespace Infivex.Authentication.Configuration
{
    public static class ConnectionConfig
    {
        private static string ConnectionString { get; set; }

        public static string GetIS4ConfigDB()
        {
            var config = new ConfigurationBuilder()
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables()
                .Build();

            ConnectionString = config.GetConnectionString("IFX_IS4_DB");

            return ConnectionString;
        }

        public static string GetIFXWBMASTERDB()
        {
            var config = new ConfigurationBuilder()
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables()
                .Build();

            ConnectionString = config.GetConnectionString("IFX_WB_MASTER_DB");

            return ConnectionString;
        }
    }
}
