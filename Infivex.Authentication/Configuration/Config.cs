﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Infivex.Authentication.Configuration
{
    public class Config
    {
        // used for the initial DB population
        // scopes define the API resources in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource(
                    "api_poc",
                    "API - POC",
                    new [] {JwtClaimTypes.Name, JwtClaimTypes.WebSite, JwtClaimTypes.Role}
                    )
                {
                     Scopes = new List<Scope>()
                     {
                         new Scope("api_poc", "API-POC", new [] { JwtClaimTypes.Name, JwtClaimTypes.WebSite, JwtClaimTypes.Role })
                     }
                }
            };
        }

        // used for the initial DB population
        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client // OpenID Connect resource owner password
                {
                    ClientId = "ROPClient",
                    ClientName = "ROPClient - POC",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials, // use the resource owner password flow + allow doing server to server API calls
                    
                    ClientSecrets =
                    {
                        new Secret("ROPClientSecret".Sha256()) // add a client secret
                    },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_poc"
                    },

                    AlwaysIncludeUserClaimsInIdToken = true,

                    // Refresh token parameters
                    RefreshTokenExpiration = TokenExpiration.Absolute,
                    AbsoluteRefreshTokenLifetime = 864000, // 10 days
                    RefreshTokenUsage = TokenUsage.ReUse,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    // End refresh token params

                    RequireConsent = false,
                    AllowOfflineAccess = true, // this allows requesting refresh tokens for long lived API access
                    AccessTokenLifetime = 3600 // 1 hour (default value). Note: clock skew for Bearer authentication is 5 minutes, so real lifetime is 5 minutes more
                }
            };
        }

        // used for the initial DB population
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            // add support for the standard openid (subject id) and profile (first name, last name etc..) scopes
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }
    }
}
