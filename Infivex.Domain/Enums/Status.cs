﻿namespace Infivex.Domain.Enums
{
    public enum Status
    {
        Inactive = 0,
        Active = 1,
        Deleted = 2
    }

}
