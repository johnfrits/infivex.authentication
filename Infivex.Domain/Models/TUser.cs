﻿using System;

namespace Infivex.Domain.Models
{
    public partial class TUser
    {
        public Guid Id { get; set; }
        public Guid FkCompanyId { get; set; }
        public int FkLanguageId { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string NationalIdentificationNumber { get; set; }
        public bool? IsBookKeeper { get; set; }
        public bool? IsCustomer { get; set; }
        public bool? IsSuperAdmin { get; set; }
        public bool? IsLocalAdmin { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public DateTime? LastPasswordChangedDate { get; set; }
        public int? FailedPasswordCount { get; set; }
        public int? FailedPasswordAttemptCount { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
    }
}
