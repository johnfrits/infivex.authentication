﻿using Infivex.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Infivex.Infrastructure.DBContext
{
    public partial class WorkbenchMasterContext : DbContext
    {
        public WorkbenchMasterContext()
        {
        }

        public WorkbenchMasterContext(DbContextOptions<WorkbenchMasterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TUser> TUser { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TUser>(entity =>
            {
                entity.ToTable("TUser");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FkCompanyId).HasColumnName("fk_company_id");

                entity.Property(e => e.FkLanguageId).HasColumnName("fk_language_id");

                entity.Property(e => e.Gender)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastLoginDate).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastPasswordChangedDate).HasColumnType("datetime");

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.MobileNumber).HasMaxLength(150);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NationalIdentificationNumber).HasMaxLength(150);

                entity.Property(e => e.PasswordHash).IsRequired();

                entity.Property(e => e.PasswordSalt).IsRequired();

                entity.Property(e => e.PhoneNumber).HasMaxLength(150);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
